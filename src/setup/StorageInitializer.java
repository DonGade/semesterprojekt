package setup;

import controller.Controller;
import model.Prisliste;
import model.Produktgruppe;

public class StorageInitializer {

	public static void main(String[] args) {
		initStorage();
	}

	/** Initialises the storage with some objects. */
	private static void initStorage() {
		Produktgruppe p = Controller.opretProduktGruppe("Flaske");
		Produktgruppe p2 = Controller.opretProduktGruppe("Fustage");
		Prisliste prisliste = Controller.opretPrisliste("Fredagsbar");
		Prisliste prisliste2 = Controller.opretPrisliste("Butik");
		Controller.opretProdukt("Forårsbryg", prisliste, 50, p);
		Controller.opretProdukt("Klosterbryg", prisliste, 50, p);

		Controller.saveStorage();
	}
}
