package storage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.Prisliste;
import model.Produkt;
import model.Produktgruppe;

public class Storage implements Serializable {
	private static Storage storage;

	public static Storage getInstance() {
		if (storage == null) {
			storage = new Storage();
		}
		return storage;
	}

	private Storage() {
	}

	// -------------------------------------------------------------------------

	private List<Produktgruppe> produktgrupper = new ArrayList<>();

	public ArrayList<Produktgruppe> getProduktgrupper() {
		return new ArrayList<Produktgruppe>(produktgrupper);
	}

	public void storeProduktgruppe(Produktgruppe produktgruppe) {
		produktgrupper.add(produktgruppe);
	}

	public void deleteProduktgruppe(Produktgruppe produktgruppe) {
		produktgrupper.remove(produktgruppe);
	}

	// ------------------------------------------------------------------------
	private List<Produkt> produkter = new ArrayList<>();

	public ArrayList<Produkt> getProdukter() {
		return new ArrayList<Produkt>(produkter);
	}

	public void storeProdukt(Produkt produkt) {
		produkter.add(produkt);
	}

	public void deleteProdukt(Produkt produkt) {
		produkter.remove(produkt);
	}

	// -------------------------------------------------------------------------
	private List<Prisliste> prislister = new ArrayList<>();

	public ArrayList<Prisliste> getPrislister() {
		return new ArrayList<Prisliste>(prislister);
	}

	public void storePrisliste(Prisliste prisliste) {
		prislister.add(prisliste);
	}

	public void deletePrisliste(Prisliste prisliste) {
		prislister.remove(prisliste);
	}

	// -------------------------------------------------------------------------

	/**
	 * Loads the storage (including all objects in storage).
	 */
	public void load() {
		try (FileInputStream fileIn = new FileInputStream("storage.ser");
				ObjectInputStream in = new ObjectInputStream(fileIn)) {
			storage = (Storage) in.readObject();
			System.out.println("Storage loaded from file storage.ser.");
		} catch (ClassNotFoundException ex) {
			System.out.println("Error loading storage object.");
			throw new RuntimeException(ex.getMessage());
		} catch (IOException ex) {
			System.out.println("Error loading storage object.");
			throw new RuntimeException(ex.getMessage());
		}
	}

	/**
	 * Saves the storage (including all objects in storage).
	 */
	public void save() {
		try (FileOutputStream fileOut = new FileOutputStream("storage.ser");
				ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
			out.writeObject(storage);
			System.out.println("Storage saved in file storage.ser.");
		} catch (IOException ex) {
			System.out.println("Error saving storage object.");
			throw new RuntimeException(ex.getMessage());
		}
	}

	/** Clears the storage. */
	public void clear() {
		storage = new Storage();
	}
}
