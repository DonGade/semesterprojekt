package model;

import java.io.Serializable;
import java.util.HashMap;

public class Prisliste implements Serializable {
	private String navn;

	private HashMap<Produkt, Double> produktPriser = new HashMap<>();

	public Prisliste(String navn) {
		this.navn = navn;
	}

	public void addProdukt(Produkt produkt, double pris) {
		if (!produktPriser.containsKey(produkt)) {
			produktPriser.put(produkt, pris);
			produkt.addPrisliste(this);
		}
	}

	public HashMap<Produkt, Double> getProduktpriser() {
		return new HashMap<>(produktPriser);
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String toString() {
		return navn;
	}
}
