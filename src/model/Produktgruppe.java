package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Produktgruppe implements Serializable {
	private String navn;
	private ArrayList<Produkt> produkter;

	public Produktgruppe(String navn) {
		this.navn = navn;
		produkter = new ArrayList<>();
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public void opretProdukt(String navn, double pris, double barPris) {

	}

	public void addProdukt(Produkt produkt) {
		if (!produkter.contains(produkt))
			produkter.add(produkt);
	}

	public ArrayList<Produkt> getProdukter() {
		return new ArrayList<>(produkter);
	}

	public String toString() {
		return navn;
	}
}
