package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Produkt implements Serializable {
	private String navn;
	private ArrayList<Prisliste> prislister = new ArrayList<>();

	public Produkt(String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public void addPrisliste(Prisliste prisliste) {
		if (!prislister.contains(prisliste)) {
			prislister.add(prisliste);
		}
	}

	public String toString() {
		return navn;
	}
}
