package gui;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import model.Prisliste;
import model.Produkt;
import model.Produktgruppe;

public class ProduktOversigtPane extends GridPane {

	private ListView<Produktgruppe> lvwProduktgrupper = new ListView<>();
	private ListView<Produkt> lvwProdukter = new ListView<>();
	private ListView<Prisliste> lvwPrislister = new ListView<>();

	private Label lblProdukt = new Label("Produkter: ");

	public ProduktOversigtPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);

		// produktgruppe
		Label lblProduktgruppe = new Label("Produktgrupper: ");
		this.add(lblProduktgruppe, 0, 0);
		this.add(lvwProduktgrupper, 0, 1);
		lvwProduktgrupper.setPrefHeight(200);
		lvwProduktgrupper.setPrefWidth(150);

		ChangeListener<Produktgruppe> pgListener = (ov, nv, pv) -> this.selectedProductgruppeChanged();
		lvwProduktgrupper.getSelectionModel().selectedItemProperty().addListener(pgListener);

		// prisliste
		Label lblPrisliste = new Label("Vælg prisliste: ");
		this.add(lblPrisliste, 2, 0);
		this.add(lvwPrislister, 2, 1);
		lvwPrislister.setPrefHeight(200);
		lvwPrislister.setPrefWidth(150);

		ChangeListener<Prisliste> plListener = (ov, nv, pv) -> this.selectedPrislisteChanged();
		lvwPrislister.getSelectionModel().selectedItemProperty().addListener(plListener);

		// produkt
		this.add(lblProdukt, 1, 0);
		this.add(lvwProdukter, 1, 1);
		lvwProdukter.setPrefHeight(300);
		lvwProdukter.setPrefWidth(150);

		//Opret produkt
		Button btnOpret = new Button("Nyt produkt");
		this.add(btnOpret, 1, 2);
		btnOpret.setOnAction(event -> this.opretAction());

		updateControls();
		lvwProduktgrupper.getSelectionModel().select(0);

	}

	private void opretAction() {
		Produktgruppe selected = lvwProduktgrupper.getSelectionModel().getSelectedItem();
		OpretProduktDialog window = new OpretProduktDialog(selected);
		window.showAndWait();
		window.hide();
		if (selected != null) {
			lvwProdukter.getItems().setAll(selected.getProdukter());
		}
	}

	private void selectedPrislisteChanged() {
		Prisliste selected = lvwPrislister.getSelectionModel().getSelectedItem();
		if (selected != null) {
			lvwProdukter.getItems().setAll(selected.getProduktpriser().keySet());
			lblProdukt.setText("Produkter: " + selected.getNavn());
			lvwProduktgrupper.getSelectionModel().clearSelection();
		}
	}

	private void selectedProductgruppeChanged() {
		Produktgruppe selected = lvwProduktgrupper.getSelectionModel().getSelectedItem();
		if (selected != null) {
			lvwProdukter.getItems().setAll(selected.getProdukter());
			lblProdukt.setText("Produkter: " + selected.getNavn());
			lvwPrislister.getSelectionModel().clearSelection();
		}
	}

	private void updateControls() {
		lvwProduktgrupper.getItems().setAll(Controller.getProduktgrupper());
		lvwPrislister.getItems().setAll(Controller.getPrislister());
	}

}
