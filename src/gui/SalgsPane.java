package gui;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import model.Prisliste;
import model.Produkt;
import model.Produktgruppe;

public class SalgsPane extends GridPane {

	private ListView<Produktgruppe> lvwProduktgrupper = new ListView<>();
	private ListView<Produkt> lvwProdukter = new ListView<>();
	private ComboBox<Prisliste> cbbPrisliste = new ComboBox<>();
	private ListView<Produkt> lvwOrdre = new ListView<>();

	private CheckBox cbxUdlejning = new CheckBox();

	private Button btnAntal1 = new Button("1");
	private Button btnAntal2 = new Button("2");
	private Button btnAntal3 = new Button("3");
	private Button btnAntal4 = new Button("4");
	private Button btnAntal5 = new Button("5");
	private Button btnAntal6 = new Button("6");
	private Button btnAntal7 = new Button("7");
	private Button btnAntal8 = new Button("8");
	private Button btnAntal9 = new Button("9");

	private Button btnTilfoej = new Button("-->");
	private Button btnFjern = new Button("<--");

	private CheckBox cbxRabatProcent = new CheckBox();
	private CheckBox cbxRabatPris = new CheckBox();
	private TextField txfRabat = new TextField();

	public SalgsPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);

		// H- og V-bokse
		VBox v1 = new VBox(10);
		v1.setPrefSize(200, 500);
		this.add(v1, 0, 0);

		VBox v2 = new VBox(10);
		v2.setPrefSize(200, 500);
		this.add(v2, 1, 0);

		VBox v3 = new VBox(10);
		v3.setPrefSize(200, 500);
		this.add(v3, 2, 0);

		VBox v4 = new VBox(10);
		v4.setPrefSize(200, 500);
		this.add(v4, 3, 0);

//----------------------------------------------------------------------------------------
		// Prisliste dropdown
		Label lblPrisliste = new Label("Vælg prisliste");
		v1.getChildren().add(lblPrisliste);
		v1.getChildren().add(cbbPrisliste);
		cbbPrisliste.getItems().setAll(Controller.getPrislister());

		ChangeListener<Prisliste> plListener = (ov, nv, pv) -> this.selectedPrisListeChanged();
		cbbPrisliste.getSelectionModel().selectedItemProperty().addListener(plListener);

		// Produktgrupper
		Label lblProduktgrupper = new Label("Vælg produktgruppe");
		v1.getChildren().add(lblProduktgrupper);
		v1.getChildren().add(lvwProduktgrupper);

		ChangeListener<Produktgruppe> pgListener = (ov, nv, pv) -> this.selectedProduktGruppeChanged();
		lvwProduktgrupper.getSelectionModel().selectedItemProperty().addListener(pgListener);
		// ----------------------------------------------------------------------------------------
		// Produkter
		Label lblProdukter = new Label("Produkter");
		v2.getChildren().add(lblProdukter);
		v2.getChildren().add(lvwProdukter);

//----------------------------------------------------------------------------------------

		GridPane buttonPane = new GridPane();
		v3.getChildren().add(buttonPane);
		buttonPane.setHgap(5);
		buttonPane.setVgap(5);

		// Tal-knappe
		Label lblAntal = new Label("Antal");
		buttonPane.add(lblAntal, 0, 0);
		buttonPane.add(btnAntal1, 0, 1);
		buttonPane.add(btnAntal2, 1, 1);
		buttonPane.add(btnAntal3, 2, 1);
		buttonPane.add(btnAntal4, 0, 2);
		buttonPane.add(btnAntal5, 1, 2);
		buttonPane.add(btnAntal6, 2, 2);
		buttonPane.add(btnAntal7, 0, 3);
		buttonPane.add(btnAntal8, 1, 3);
		buttonPane.add(btnAntal9, 2, 3);
		btnAntal1.setPrefSize(50, 50);
		btnAntal2.setPrefSize(50, 50);
		btnAntal3.setPrefSize(50, 50);
		btnAntal4.setPrefSize(50, 50);
		btnAntal5.setPrefSize(50, 50);
		btnAntal6.setPrefSize(50, 50);
		btnAntal7.setPrefSize(50, 50);
		btnAntal8.setPrefSize(50, 50);
		btnAntal9.setPrefSize(50, 50);
		// TODO
		// Mangler funktioner

		// Tilføj og fjern
		buttonPane.add(btnTilfoej, 1, 4);
		buttonPane.add(btnFjern, 1, 5);
		// TODO
		// Mangler funktioner

		// Udlejning
		Label lblUdlejning = new Label("Er udlejning");
		buttonPane.add(lblUdlejning, 1, 7, 2, 1);
		buttonPane.add(cbxUdlejning, 1, 8);
		cbxUdlejning.setOnMouseClicked(event -> this.erUdlejning());
		// TODO
		// Mangler funktioner

		// Rabatter
		Label lblProcentRabat = new Label("Rabat i procent");
		Label lblFastRabat = new Label("Sæt pris");

		buttonPane.add(lblProcentRabat, 0, 9, 2, 1);
		buttonPane.add(cbxRabatProcent, 0, 10);
		buttonPane.add(lblFastRabat, 2, 9);
		buttonPane.add(cbxRabatPris, 2, 10);
		buttonPane.add(txfRabat, 0, 12, 3, 1);
		txfRabat.setDisable(true);
		cbxRabatPris.setOnMouseClicked(event -> selectRabat());
		cbxRabatProcent.setOnMouseClicked(event -> selectRabat());

		// TODO
		// Mangler funktioner

//----------------------------------------------------------------------------------------

		// Ordre
		Label lblOrdre = new Label("Ordre");
		v4.getChildren().add(lblOrdre);
		v4.getChildren().add(lvwOrdre);
		// TODO
		// Metode

//----------------------------------------------------------------------------------------

	}

	private void selectedPrisListeChanged() {
		Prisliste selected = cbbPrisliste.getSelectionModel().getSelectedItem();
		if (selected != null) {
//			lvwProduktgrupper.getItems().setAll(Controller.getProduktgrupper);
		}
	}

	private void selectedProduktGruppeChanged() {
		Produktgruppe selected = lvwProduktgrupper.getSelectionModel().getSelectedItem();
		if (selected != null) {
			lvwProdukter.getItems().setAll(selected.getProdukter());
		}
	}

	private void selectedNumber() {

	}

	private void erUdlejning() {
		if (cbxUdlejning.isSelected()) {
			// TODO
			// Prisudregning
		}
	}

	private void selectRabat() {
		if (cbxRabatPris.isSelected()) {
			if (cbxRabatProcent.isDisabled()) {
				cbxRabatProcent.setDisable(false);
				txfRabat.setDisable(true);
			}
			if (!cbxRabatProcent.isDisabled()) {
				cbxRabatProcent.setDisable(true);
				txfRabat.setDisable(false);
			}
		}
		if (cbxRabatProcent.isSelected()) {
			cbxRabatPris.setDisable(true);
			txfRabat.setDisable(false);
		}

	}
}