package gui;

import controller.Controller;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void init() throws Exception {
		Controller.loadStorage();
	}

	@Override
	public void start(Stage stage) {
		BorderPane pane = new BorderPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setTitle("Århus Bryghus");
		stage.setScene(scene);
		// stage.setHeight(600);
		// stage.setWidth(750);
		stage.show();
	}

	@Override
	public void stop() throws Exception {
		Controller.saveStorage();
	}

	private void initContent(BorderPane pane) {
		TabPane tabPane = new TabPane();
		this.initTabPane(tabPane);
		pane.setCenter(tabPane);

	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		Tab tabProduktOversigt = new Tab("Produktoversigt");
		Tab tabSalg = new Tab("Salg");

		tabProduktOversigt.setContent(new ProduktOversigtPane());
		tabSalg.setContent(new SalgsPane());

		tabPane.getTabs().addAll(tabProduktOversigt, tabSalg);

	}

}
