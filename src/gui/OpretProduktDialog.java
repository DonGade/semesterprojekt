package gui;

import controller.Controller;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Produkt;
import model.Produktgruppe;

public class OpretProduktDialog extends Stage {
	private Produktgruppe produktgruppe;

	public OpretProduktDialog(Produktgruppe produktgruppe) {
		this.produktgruppe = produktgruppe;

		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);
		this.setTitle("Opret nyt produkt");

		GridPane pane = new GridPane();
		Scene scene = new Scene(pane);
		this.initContent(pane);
		this.setScene(scene);
	}

	private ComboBox<Produktgruppe> cbxProduktgrupper = new ComboBox<>();
	private TextField txfNavn = new TextField();
	private ListView<CheckBox> lvwPrislister = new ListView<>();
	private ListView<TextField> lvwPriser = new ListView<>();

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setHgap(20);
		pane.setVgap(10);

		Label lblProduktgruppe = new Label("Produktgruppe: ");
		pane.add(lblProduktgruppe, 0, 0);
		pane.add(cbxProduktgrupper, 1, 0);
		cbxProduktgrupper.getItems().setAll(Controller.getProduktgrupper());
		if (produktgruppe != null) {
			cbxProduktgrupper.getSelectionModel().select(produktgruppe);
		}

		Label lblNavn = new Label("Navn: ");
		pane.add(lblNavn, 0, 1);
		pane.add(txfNavn, 1, 1);

		for (int i = 0; i < Controller.getPrislister().size(); i++) {
			lvwPrislister.getItems().add(new CheckBox(Controller.getPrislister().get(i).getNavn()));
			TextField txfPris = new TextField();
			txfPris.setPrefWidth(40);
			lvwPriser.getItems().add(txfPris);
		}

		Label lblPrislister = new Label("Tilføj prislister: ");
		lvwPrislister.setPrefHeight(90);
		lvwPrislister.setPrefWidth(100);
		lvwPriser.setPrefSize(50, 100);
		pane.add(lblPrislister, 0, 2);
		HBox lvwBox = new HBox(lvwPrislister, lvwPriser);
		pane.add(lvwBox, 1, 2);

		Button btnAdd = new Button("Opret");
		Button btnFortryd = new Button("Fortryd");
		HBox btnBox = new HBox(0, btnFortryd, btnAdd);
		pane.add(btnBox, 1, 3);
		btnFortryd.setOnAction(event -> this.close());
		btnAdd.setOnAction(event -> this.addAction());

	}

	private void addAction() {
		Produktgruppe selected = cbxProduktgrupper.getSelectionModel().getSelectedItem();
		if (selected != null) {
			Produkt produkt = Controller.opretProdukt(txfNavn.getText(), selected);
			for (int i = 0; i < lvwPrislister.getItems().size(); i++) {
				if (lvwPrislister.getItems().get(i).isSelected()) {
					double pris = Double.parseDouble(lvwPriser.getItems().get(i).getText());
					Controller.getPrislister().get(i).addProdukt(produkt, pris);
				}
			}
		}
		this.close();
	}
}
