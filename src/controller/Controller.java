package controller;

import java.util.ArrayList;

import model.Prisliste;
import model.Produkt;
import model.Produktgruppe;
import storage.Storage;

public class Controller {

	public static Produktgruppe opretProduktGruppe(String navn) {
		Produktgruppe p = new Produktgruppe(navn);
		Storage.getInstance().storeProduktgruppe(p);
		return p;
	}

	public static Produkt opretProdukt(String navn, Prisliste prisliste, double pris, Produktgruppe produktgruppe) {
		Produkt produkt = new Produkt(navn);
		prisliste.addProdukt(produkt, pris);
		produktgruppe.addProdukt(produkt);
		Storage.getInstance().storeProdukt(produkt);
		System.out.println(Storage.getInstance().getProdukter());
		return produkt;
	}

	public static Produkt opretProdukt(String navn, Produktgruppe produktgruppe) {
		Produkt produkt = new Produkt(navn);
		produktgruppe.addProdukt(produkt);
		Storage.getInstance().storeProdukt(produkt);
		return produkt;
	}

	public static Prisliste opretPrisliste(String navn) {
		Prisliste prisliste = new Prisliste(navn);
		Storage.getInstance().storePrisliste(prisliste);
		return prisliste;
	}

	public static ArrayList<Produktgruppe> getProduktgrupper() {
		return Storage.getInstance().getProduktgrupper();
	}

	public static ArrayList<Prisliste> getPrislister() {
		return Storage.getInstance().getPrislister();
	}

	public static void loadStorage() {
		Storage.getInstance().load();
	}

	public static void saveStorage() {
		Storage.getInstance().save();
	}

	public static void clearStorage() {
		Storage.getInstance().clear();
	}
}
